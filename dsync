#!/usr/bin/python
import os
import json
import time
import sys
from StringIO import StringIO
from hashlib import md5
from ftplib import FTP

CHECK_WHOLE_FILE = 0

class RemoteFTP:
    def __init__(self, config):
        self.config = config
        self.ready = False
        self.target_path = None
        self.ftp = None
        self.setTargetPath(config["repo"])

    def connect(self):
        if self.ready:
            return False
        passwd = self.config["pass"]
        if passwd == "":
            passwd = raw_input("Password: ")
        print "Connecting to %(host)s/%(path)s ... " % self.config,
        self.ftp = FTP(self.config["host"])
        self.ftp.login(self.config["login"], passwd)
        self.ftp.cwd(self.config["path"])
        self.ready = True
        print " [OK]"


    def quit(self):
        if self.ready:
            self.ftp.quit()
        self.ready = False

    def setTargetPath(self, pth):
        self.target_path = pth

    def uploadFile(self, fname, source):
        self.connect()
        fp = open(source, "rb")
        remote_path = self.target_path + "/" + fname
        self.ftp.storbinary("STOR %s" % remote_path, fp)
        fp.close()

    def downloadFile(self, fname, target):
        self.connect()
        try:
            os.makedirs(os.path.dirname(target))
        except:
            pass
        fp = open(target, "wb")
        remote_path = self.target_path + "/" + fname
        self.ftp.retrbinary("RETR %s" % remote_path, fp.write)
        fp.close()

    def deleteFile(self, fname):
        self.connect()
        remote_path = self.target_path + "/" + fname
        self.ftp.sendcmd('SITE CHMOD 777 %s' % remote_path)
        try:
            self.ftp.delete(remote_path)
        except:
            print "Failed to delete file %s" % remote_path
            pass

    def mkdir(self, path):
        self.connect()
        self.ftp.mkd(self.target_path + path)

    def rmdir(self, path):
        self.connect()
        self.ftp.rmd(self.target_path + path)

'''
    helper functions
'''
def loadConfig():
    fp = open(".dsync", "rb")
    data = json.loads(fp.read())
    fp.close()
    return data

def saveConfig(config):
    fp = open(".dsync", "wb")
    data = json.dumps(config)
    fp.write(data)
    fp.close()



def getFileHash(path):
    #print path
    fp = open(path, "rb")
    size = os.stat(path).st_size
    if size <= 1024*10:
        hsh = md5(fp.read()).hexdigest()
    else:
        # more complex checking
        thsh = []
        parts = size / 512
        for p in xrange(parts):
            fp.seek(p * 512)
            thsh.append(fp.read(64))
        hsh = md5('|'.join(thsh)).hexdigest()
    fp.close()
    return hsh

def transRemoteFname(fname):
    hsh = md5(fname).hexdigest()
    fname = fname.replace("/", "_")
    fname = fname.replace(".", "_")
    fname = fname.replace("\\", "_")
    return "%s.%s.stor" % (hsh[:8], fname)

def _walkFiles(abspath, localpath, result):
    files = os.listdir(abspath)
    for fname in files:
        if fname == ".dsync" or fname == ".dsync-remote.tmp":
            continue
        if fname == ".DS_Store":
            continue
        fpath = "/".join((localpath, fname))
        fabspath = os.path.join(abspath, fname)

        if os.path.isdir(fabspath):
            _walkFiles(fabspath, fpath, result)
        else:
            result[fpath] = [
                os.path.getsize(fabspath),
                os.path.getmtime(fabspath),
                None,
                fabspath,
                ]

def checkFname(fname, excls):
    for e in excls:
        if e in fname:
            return False
    return True


def getLocalFList(config):
    exclfiles = config.get("exclude", None)
    abspath = os.path.abspath(".")
    files_list = {}
    _walkFiles(abspath, "", files_list)
    if exclfiles:
        result = {}
        for f in files_list:
            if checkFname(f, exclfiles):
                result[f] = files_list[f]
        return result
    else:
        return files_list

def loadRemoteRepo(rmt):
    tmpname = ".dsync-remote.tmp"
    try:
        rmt.downloadFile("dsync.repo", tmpname)
        remote_repo = json.load(open(tmpname))
        os.remove(tmpname)
    except:
        print "Remote flist not found or damaged."
        r = raw_input("Do you want to continue? Y/n")
        remote_repo = {"files": {}, "rev": 0}
        if r == "n":
            raise SystemExit
    try:
        os.remove(tmpname)
    except:pass
    return remote_repo

def saveRemoteRepo(rmt, repo):
    tmpname = ".dsync-remote.tmp"
    data = json.dumps(repo)
    fp = open(tmpname, "wb")
    fp.write(data)
    fp.close()
    rmt.uploadFile("dsync.repo", tmpname)
    os.remove(tmpname)

def createRemoteFlist(local_flist, remote_flist):
    new_flist = {}
    for f in local_flist:
        fsize = local_flist[f][0]
        ftime = local_flist[f][1]
        fhash = local_flist[f][2]
        if fhash is None:
            fhash = remote_flist[f][2]
        if fhash is None:
            fhash = getFileHash(local_flist[f][3])
        new_flist[f] = (fsize, ftime, fhash)
    return new_flist

def getFlistDiff(remote_flist, local_flist):
    lfiles = local_flist.keys()
    rfiles = remote_flist.keys()

    new_local = []
    new_remote = []
    updated = []

    tocheck = []
    for f in lfiles:
        if not f in rfiles:
            new_local.append(f)
        else:
            tocheck.append(f)
    for f in rfiles:
        if not f in lfiles:
            new_remote.append(f)

    for f in tocheck:
        rf = remote_flist[f]
        lf = local_flist[f]

        if rf[0] != lf[0]:#check size diff
            updated.append(f)
        else:
            if lf[2] == None:
                lf[2] = getFileHash(lf[3])
            if rf[2] != lf[2]:# hash diff
                updated.append(f)

    return (new_local, new_remote, updated)


def print_diff(remote_flist, local_flist):
    nl, nr, up = getFlistDiff(remote_flist, local_flist)
    for f in nl:
        print "L %s" % f
    for f in nr:
        print "R %s" % f
    for f in up:
        print "U %s" % f

def check_version(config, remote_repo):
    local_ver = config["rev"]
    remote_ver = remote_repo["rev"]

    if local_ver < remote_ver:
        return -1
    if local_ver == remote_ver:
        return 1

    return 0

'''
    commands
'''
def cmd_init(try_conn=True):
    config = {"rev": 0}
    config["host"] = raw_input("Remote host: ")
    config["login"] = raw_input("Login: ")
    config["pass"] = raw_input("Password: ")
    config["path"] = raw_input("Path: ")
    repo_id = os.path.basename(os.getcwd())
    config["repo"] = raw_input("Repo name[%s]: " % repo_id)
    if config["repo"] == "":
        config["repo"] = repo_id

    fp = open(".dsync", "wb")
    sd = json.dumps(config)
    fp.write(sd)
    fp.close()

    if try_conn:
        rmt = RemoteFTP(config)
        rmt.connect()
        rmt.quit()
    return config


def cmd_create():
    config = cmd_init(False)
    rmt = RemoteFTP(config)
    rmt.connect()
    print "Creating remote repository: %s" % config["repo"]
    rmt.mkdir("")
    rmt.quit()

def cmd_push(attrs=None):
    if attrs is None:
        config = loadConfig()
        rmt = RemoteFTP(config)
    else:
        config, rmt = attrs

    local_flist = getLocalFList(config)
    remote_repo = loadRemoteRepo(rmt)
    remote_flist = remote_repo["files"]

    verdiff = check_version(config, remote_repo)
    if verdiff == -1:
        r = raw_input("WARNING: Local version is older than remote. Do you want to continue? N/y")
        if r != "y":
            rmt.quit()
            raise SystemExit


    nl, nr, up = getFlistDiff(remote_flist, local_flist)
    changes = len(nl) + len(nr) + len(up)
    if changes > 0:
        config["rev"] += 1
        print "Push revision: %s" % config["rev"]

        for f in nl:
            print "UPLOAD %s" % f
            labsf = local_flist[f][3]
            local_flist[f][2] = getFileHash(labsf)
            rname = transRemoteFname(f)
            rmt.uploadFile(rname, labsf)

        for f in nr:
            print "REMOVE %s" % f
            rname = transRemoteFname(f)
            rmt.deleteFile(rname)

        for f in up:
            print "UPLOAD %s" % f
            labsf = local_flist[f][3]
            local_flist[f][2] = getFileHash(labsf)
            rname = transRemoteFname(f)
            rmt.uploadFile(rname, labsf)

        rflist_new = createRemoteFlist(local_flist, remote_flist)
        remote_repo = {"files": rflist_new, "rev": config["rev"]}
        saveRemoteRepo(rmt, remote_repo)
        saveConfig(config)
    else:
        print "No changes"
    rmt.quit()

def cmd_pull(attrs=None):
    if attrs is None:
        config = loadConfig()
        rmt = RemoteFTP(config)
    else:
        config, rmt = attrs
    local_flist = getLocalFList(config)
    remote_repo = loadRemoteRepo(rmt)
    remote_flist = remote_repo["files"]
    nl, nr, up = getFlistDiff(remote_flist, local_flist)
    changes = len(nl) + len(nr) + len(up)
    config["rev"] = remote_repo["rev"]
    if changes > 0:
        print "Pull revision: %s" % config["rev"]

        for f in nl:
            print "REMOVE %s" % f
            try:
                os.remove(f[1:])
            except:
                pass

        for f in nr:
            print "DOWNLOAD %s" % f
            rname = transRemoteFname(f)
            rmt.downloadFile(rname, f[1:])

        for f in up:
            print "DOWNLOAD %s" % f
            rname = transRemoteFname(f)
            rmt.downloadFile(rname, f[1:])

        saveConfig(config)
    else:
        print "No changes"
    rmt.quit()



def cmd_status():
    config = loadConfig()
    rmt = RemoteFTP(config)
    local_flist = getLocalFList(config)
    remote_repo = loadRemoteRepo(rmt)
    remote_flist = remote_repo["files"]
    print "Local revision: %d" % config["rev"]
    print "Remote revision: %d" % remote_repo["rev"]
    print_diff(remote_flist, local_flist)
    rmt.quit()


def cmd_sync():
    config = loadConfig()
    rmt = RemoteFTP(config)
    attrs = (config, rmt)
    local_flist = getLocalFList(config)
    remote_repo = loadRemoteRepo(rmt)
    remote_flist = remote_repo["files"]

    verdiff = check_version(config, remote_repo)

    if verdiff == -1:
        # update
        cmd_pull(attrs)
    else:
        nl, nr, up = getFlistDiff(remote_flist, local_flist)
        changes = len(nl) + len(nr) + len(up)
        if changes > 0:
            cmd_push(attrs)
        else:
            print "Nothing changed."
    rmt.quit()




try:
    cmd = sys.argv[1]
except:
    cmd = "sync"
    pass

if cmd == "init":
    cmd_init()
elif cmd == "create":
    cmd_create()
elif cmd == "push":
    cmd_push()
elif cmd == "pull":
    cmd_pull()
elif cmd == "st" or cmd == "status":
    cmd_status()
elif cmd == "sync":
    cmd_sync()
else:
    print "Usage:"
    print "  dsync init - init repo in current dir"
    print "  dsync create - init repo in current dir and create remote"
    print "  dsync [sync]- synchronize local and remote"
    print "  dsync push - push local state to remote"
    print "  dsync pull - pull remote state to local"
    print "  dsync st/state - print changes"
