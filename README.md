# dsync

ftp files synchronization

### Installation

* download and install Python 2.7
* `$ git clone https://gindar@bitbucket.org/gindar/dsync.git`
* `$ cd dsync`
* `$ python setup.py install` as root

Note: On windows copy dsync file directly to your dsync repository dir.

### Usage

** Create new repository **

    $ dsync create
    > Remote host: ftp.domain.com
    > Login: ftplogin
    > Password: abc123
    > Path: remote/path/to/repo
    > Repo name[abc]: cba
    $ dsync


** Checkout existing repository **

    $ dsync init
    > Remote host: ftp.domain.com
    > Login: ftplogin
    > Password: abc123
    > Path: remote/path/to/repo
    > Repo name[abc]: cba

** Synchronise local copy with remote. **

 * Automatic `$ dsync`
 * Force push `$ dsync push`
 * Force pull `$ dsync pull`

** For more info type: `$ dsync help` **